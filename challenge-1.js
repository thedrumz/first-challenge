/*

* * * * * * * * * * * * * * * *
*  1. C A L C U L A D O R A   *
* * * * * * * * * * * * * * * *

Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones. 

    - El programa debe recibir dos números (n1, n2).

    - Debe existir una variable que permita seleccionar de alguna forma el tipo de operación (suma, resta, multiplicación 
      o división).

    - Opcional: agrega una operación que permita elevar n1 a la potencia n2.

*/

const SUM = 'sum';
const SUBTRACTION = 'subtraction';
const MULTIPLICATION = 'multiplication';
const DIVISION = 'division';
const POWER = 'power';

const operator = {
  sum: (n1, n2) => n1 + n2,
  subtraction: (n1, n2) => n1 - n2,
  multiplication: (n1, n2) => n1 * n2,
  division: (n1, n2) => n1 / n2,
  power: (n1, n2) => n1 ** n2
}

const calculator = (n1, n2, operation) => {
  return operator[operation](n1, n2);
}

// Test cases
console.log(calculator(2,3, SUM));
console.log(calculator(2,3, SUBTRACTION));
console.log(calculator(2,3, MULTIPLICATION));
console.log(calculator(2,3, DIVISION));
console.log(calculator(2,3, POWER));