/*

* * * * * * * * * * * * * * * * * * * * * * *
*  3. ASINCRONIA                            *
* * * * * * * * * * * * * * * * * * * * * * *

En este ejercicio se comprobará la competencia de los alumnos en el concepto de asincroní­a 
Se proporcionan 3 archivos csv separados por comas y se deberán bajar asíncronamente (promises) 

A la salida se juntarán los registros de los 3 archivos en un array que será el parámetro de entrada 
de la funcion findIPbyName(array, name ,surname) que buscará una entrada en el array y devolverá la IP correspondiente

Una vez hallada la IP ha de mostrarse por pantalla

para llamar a la función utilizad el nombre Cari Wederell

*/

const fsPromises = require('fs').promises;

const filePaths = [
  'resources/MOCK_DATA1.csv', 
  'resources/MOCK_DATA2.csv', 
  'resources/MOCK_DATA3.csv'
];

const NAME_POSITION = 1;
const SURNAME_POSITION = 2;
const IP_POSITION = 5;

// Test cases
const FIRST_NAME = 'Cari';
const LAST_NAME = 'Wederell'

const findIPbyName = (array, name ,surname) => {
  const findedLine = array.find(line => line[NAME_POSITION] == name && line[SURNAME_POSITION] == surname);

  if(findedLine == undefined) {
    return false;
  }

  return findedLine[IP_POSITION];
}

const processFile = (file) => {
  return file
    .toString()
    .split('\n')
    .map(string => string.split(','));
}

const getFiles = (filePaths) => filePaths.map(file => fsPromises.readFile(file));

Promise.all(getFiles(filePaths))
.then(processFile)
.then(array => findIPbyName(array, FIRST_NAME, LAST_NAME))
.then(finded => console.log(finded))
.catch(err => console.log(err));